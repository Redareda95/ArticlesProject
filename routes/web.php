<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/contact-us', function() {
	return view('contact');
});

Route::get('/about-us', function() {
	return view('about');
});

//Show all news
Route::get('/allnews', 'ArticleController@index');
Route::get('/article/{id}', 'ArticleController@show');

//Add Comment
Route::post('/article/{articles}/comment', 'CommentsController@store');

//Create new Article
Route::get('/create', 'ArticleController@create');
Route::post('/articles', 'ArticleController@store');

//Show Join Credentials
Route::get('/join-Us', 'RegisterationController@show')->name('login');

//Auth:register
Route::post('/register', 'RegisterationController@register');

//Auth:login
Route::post('/login', 'LoginController@login');

//Auth logout
Route::get('/logout', 'LoginController@logout');

//Admin panel
Route::group(['middleware'=>'roles', 'roles'=>'admin'], function(){
	Route::get('/admin', 'AdminsController@showPanel');
	Route::get('/users', 'AdminsController@showUsers');
	Route::post('/add-roles', 'AdminsController@addRole');
	Route::get('/addUser', 'AdminsController@addUser');
	Route::post('/registerFromPanel', 'AdminsController@register');
	Route::get('/articlesAdmin', 'AdminsController@showArticles');
	Route::get('/articles/edit/{id}', 'AdminsController@showEditPage');
	Route::patch('/update/{id}', 'AdminsController@update');
	Route::get('/articles/delete/{id}', 'AdminsController@destroyArticle');
	Route::get('/pending-articles', 'AdminsController@showPendingArticles');
	Route::get('/changeArticleStatus/{id}', 'AdminsController@changeArticleStatus');
	Route::get('/pending-comments', 'AdminsController@showPendingComments');
	Route::get('/changeCommentStatus/{id}', 'AdminsController@changeCommentStatus');
	Route::get('/comment/delete/{id}', 'AdminsController@destroyComment');

});
