<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function __construct()
	{
       $this->middleware('guest')->except(['logout']);
	}

	public function login()
    {
    	if(! auth()->attempt(request(['email','password'])))
    	{
    		return back()->withErrors(['message'=>'Not correct informations']);
    	}

        session()->flash('message', 'Hello ' . \Auth::user()->name . ' ! you have Login Successfully...');

    	return redirect('/admin');
    }

    public function logout()
    {
    	auth()->logout();
    	session()->flash('message', 'You Have Successfully Logged Out');
    	return redirect('/');
    }
}
