<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\requests\registerationForm;
use App\User;
use App\Role;

class RegisterationController extends Controller
{
     public function __construct ()
    {
       $this->middleware('guest');
    }

    public function show ()
    {
		return view('sessions.credentials');    
	}

	public function register (Request $request)
    {
    	$this->validate(request(),[
            'name'=>'required|min:5|max:25',
            'email'=>'required|email',
            'password'=>'required|confirmed'
        ]);

    	$pass=request(['password']);
        $user= new User;
        $user->name=request('name');
        $user->email=request('email');
        $user->password=bcrypt($pass['password']);
        $user->save();

        //to asign user role for a new user
        $user->roles()->attach(Role::where('name', 'user')->first());

        auth()->login($user);

        session()->flash('message', 'Congratulates! you have Login Successfully...');

    	return redirect('/');
    }
}
