<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Articles;
use App\User;

class Comments extends Model
{
    protected $fillable = ['body', 'articles_id', 'user_id'];

    public function article()
    {
        return $this->belongsTo(Articles::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
