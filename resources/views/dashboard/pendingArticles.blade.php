@extends('dashboard.masterAdmin')
@section('admin')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
										<h2>Pending Articles</h2>
										<hr>
@if($flash = session('message'))
	<div class="alert alert-warning" role="alert">
		<b>{{ $flash }}</b>
	</div>	
@endif

	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<table class="table table-hover">
			<tr>
				<th>Id</th>
				<th>title</th>
				<th>Author</th>
				<th>Created At</th>
				<th>Publish</th>
				<th>Delete</th>
				<th>Display</th>
			</tr>
				@foreach($articles as $article)
			<tr>
				<td>{{ $article->id }}</td>
				<td>{{ $article->title }}</td>
				<td>{{ $article->user->name }}</td>
				<td>{{ $article->created_at->toFormattedDateString() }}</td>
				<td><a href="/changeArticleStatus/{{ $article->id }}"><button class="btn btn-info">Publsih</button></a></td>
				<td><a href="/articles/delete/{{ $article->id }}"><button class="btn btn-danger">Delete</button></a></td>
				<td><a href="/article/{{ $article->id }}"><button class="btn btn-warning">Display</button></a></td>
			</tr>
				@endforeach			
		</table>
	

	</div>
</main>
@endsection
