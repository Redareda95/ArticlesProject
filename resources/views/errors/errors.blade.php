	@if(count($errors))
		<div class="alert alert-danger" style="margin: 45px auto">
			<ul>
				@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif